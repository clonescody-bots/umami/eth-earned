const axios = require("axios").default;

const { Client, Intents } = require("discord.js");
const { config } = require("./config");

const client = new Client({ intents: [Intents.FLAGS.GUILDS] });
// if you don't want to update a server ran by a bozo
const kekList = ["885769815552323626"];

const updateValues = async () => {
  try {
    const responseMetrics = await axios.get(
      `${config.UMAMI_API_URL}/staking/metrics/current?keys=total-eth`
    );

    const totalHistoryWeth = parseFloat(
      responseMetrics.data.metrics[0].value
    ).toFixed(2);

    client.guilds.cache.map((guild) => {
      // update every server the bot is connected to
      if (guild.me) {
        const nickname = kekList.includes(guild.id.toString())
          ? "ON STRIKE"
          : `${parseFloat(totalHistoryWeth).toLocaleString()}`;
        guild.me.guild.me.setNickname(nickname);
        client.user.setActivity("Total ETH earned", {
          type: "WATCHING",
        });
      }
    });
  } catch (error) {
    console.log("error : ", error);
  }
};

const run = async () => {
  // update once to init then run every minute
  await updateValues();
  setInterval(updateValues, 600000);
};

client.once("ready", () => {
  console.log("Ready!");

  run();
});

client.login(config.DISCORD_CLIENT_TOKEN);
