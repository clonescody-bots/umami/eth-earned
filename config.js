exports.config = {
  DISCORD_CLIENT_TOKEN: process.env.DISCORD_CLIENT_TOKEN,
  UMAMI_API_URL: "https://api.umami.finance/api/v2",
};
